import { Fragment, useCallback, useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import styled, { css, keyframes } from 'styled-components';
import { EndScreen } from "../components";
import Swal from "sweetalert2";

interface Question {
  correct_answer: string,
  incorrect_answers: string[],
  question: string,
}

const QuestionScreen: React.FC = () => {

  const location = useLocation();
  const selectedCategory = location.state.category;
  const selectedDifficulty = location.state.difficulty;
  const [questions, setQuestions] = useState<Question[]>([]);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const [answers, setAnswers] = useState<string[]>([]);
  const [score, setScore] = useState(0);
  const [isCorrect, setIsCorrect] = useState<boolean | null>(null);
  const [selectedAnswerIndex, setSelectedAnswerIndex] = useState<number | null>(null);
  const [timer, setTimer] = useState(60);
  const [isAnswerSelected, setIsAnswerSelected] = useState<boolean>(false);
  const [isFiftyFiftyUsed, setFiftyFiftyUsed] = useState<boolean>(false);
  const [isFriendCalled, setIsFriendCalled] = useState<boolean>(false);
  const [isAudienceAsked, setIsAudienceAsked] = useState<boolean>(false);
  const [gameOver, setGameOver] = useState<boolean>(false);

  const shuffleArray = (array: string[]) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]]
    }
    return array;
  };

  useEffect(() => {
    if (isCorrect === false) {
      setTimeout(() => {
        setGameOver(true);
      }, 3000);
    }
  }, [isCorrect]);

  useEffect(() => {
    fetch(`https://opentdb.com/api.php?amount=15&type=multiple&category=${selectedCategory}&difficulty=${selectedDifficulty}`)
      .then((res) => res.json())
      .then((data) => setQuestions(data.results));
  }, [selectedCategory, selectedDifficulty]);

  useEffect(() => {
    if (questions[currentQuestion]) {
      const allAnswers = [
        questions[currentQuestion].correct_answer,
        ...questions[currentQuestion].incorrect_answers,
      ];
      setAnswers(shuffleArray(allAnswers));
    }
  }, [questions, currentQuestion]);

  const handleNextQuestion = useCallback(() => {
    if (currentQuestion + 1 < questions.length) {
      if (isCorrect) {
        setCurrentQuestion(currentQuestion + 1);
        setIsCorrect(null);
        setTimer(60);
        setIsAnswerSelected(false);
      }
    } else {
      setGameOver(true);
    }
  }, [currentQuestion, questions.length, isCorrect]);

  useEffect(() => {
    if (timer > 0) {
      const interval = setInterval(() => {
        setTimer(isCorrect == null ? timer - 1 : timer);
      }, 1000);

      return () => clearInterval(interval);
    } else {
      Swal.fire({
        icon: 'error',
        title: 'GAME OVER!'
      })
      setGameOver(true);
    }
  }, [timer, isCorrect])

  const handleJoker = (jokerType: string) => {
    if (jokerType === "50/50" && !isFiftyFiftyUsed) {
      const correctAnswer = questions[currentQuestion].correct_answer;
      const incorrectAnswers = questions[currentQuestion].incorrect_answers;
      const remainingIncorrAnswers = incorrectAnswers.slice(0, 1);
      const newAnswers = [correctAnswer, ...remainingIncorrAnswers];
      setAnswers(shuffleArray(newAnswers));
      setFiftyFiftyUsed(true);
    }

    if (jokerType === "friend") {
      const adviceIndex = Math.floor(Math.random() * answers.length);
      const advice = answers[adviceIndex];
      setIsFriendCalled(true);
      Swal.fire({
        icon: 'question',
        title: 'Your friend suggests:',
        text: `${advice}`,
      })
    }

    if (jokerType === "audience") {
      const audiencePoll = answers.reduce((poll, answer) => {
        poll[answer] = Math.floor(Math.random() * 101);
        return poll;
      }, {} as { [key: string]: number });
      setIsAudienceAsked(true);
      Swal.fire({
        icon: 'question',
        text: `${JSON.stringify(audiencePoll)}`,
      })
    }
  };

  const handleAnswerClick = (selectedAnswer: string, index: number) => {
    if (!isAnswerSelected) {
      const isAnswerCorrect = selectedAnswer === questions[currentQuestion].correct_answer;
      setIsCorrect(isAnswerCorrect);
      setSelectedAnswerIndex(index);
      setIsAnswerSelected(true);

      if (isAnswerCorrect) {
        setScore(score + 1);
      }
    }
  };

  if (gameOver) {
    return <EndScreen gameOverReason={!isCorrect && timer > 0 ? 'Wrong answer!' : 'Time`s up!'} correctAnswers={score} />;
  }

  return (
    <Fragment>
      <GameContainer>
        <h1>Question {currentQuestion + 1} </h1>
        <QuestionContainer>
          <p dangerouslySetInnerHTML={{ __html: questions[currentQuestion]?.question }}></p>
        </QuestionContainer>
        <AnswersContainer>
          {answers.map((answer, index) => (
            <AnswerBtn
              key={index}
              onClick={() => handleAnswerClick(answer, index)}
              dangerouslySetInnerHTML={{ __html: answer }}
              isCorrect={selectedAnswerIndex === index ? isCorrect : null}
              isActualCorrect={answer === questions[currentQuestion]?.correct_answer && isCorrect === false}
            >
            </AnswerBtn>
          ))}
          {isCorrect === true && <button onClick={handleNextQuestion}>Next</button>}
        </AnswersContainer>
      </GameContainer>
      <ActionButtons>
        <JokerButton disabled={isFiftyFiftyUsed} onClick={() => handleJoker("50/50")}>50/50</JokerButton>
        <JokerButton disabled={isFriendCalled} onClick={() => handleJoker("friend")}>Call a friend</JokerButton>
        <JokerButton disabled={isAudienceAsked} onClick={() => handleJoker("audience")}>Ask the audience</JokerButton>
      </ActionButtons>
      <Timer>{timer}</Timer>
    </Fragment>
  );
};

export default QuestionScreen;

const GameContainer = styled.div`
 display: flex;
 justify-content: center;
 flex-direction: column;
 flex-wrap: wrap;
 align-content: space-around;
 align-items: center;
 position: absolute;
 top: 50%; 
 left: 50%; 
 transform: translate(-50%, -50%);
 flex-wrap: wrap;
 align-content: space-around;
`;

const ActionButtons = styled.div`
 position: absolute;
 right: 10px;
 top: 10px;
`;

const AnswersContainer = styled.div`
 display: flex;
 justify-content: center;
 flex-wrap: wrap;
 width: 100%;
 margin: 0 auto;
`;

const blinkAnimation = keyframes`
  0% { opacity: 1; }
  50% { opacity: 0.4; }
  100% { opacity: 1; }
`;

const blinkCorrectAnswer = keyframes`
  0% { background: green; }
  50% { background: orange; }
  100% { background: green; }
`;

const AnswerBtn = styled.button<{ isCorrect?: boolean | null; isActualCorrect?: boolean }>`
  width: calc(50% - 20px);
  padding: 10px;
  margin: 10px;
  text-align: center;
  background: linear-gradient(#0e0124, #22074d);
  border: 1px solid white;
  border-radius: 15px;
  font-weight: 300;
  font-size: 20px;
  cursor: pointer;
  ${({ isCorrect }) => isCorrect !== null && css`
    background: ${isCorrect ? 'green' : 'red'};
    animation: ${isCorrect ? blinkCorrectAnswer : blinkAnimation} 1s linear infinite;
  `}
  ${({ isActualCorrect }) => isActualCorrect && css`
    background: green;
    animation: ${blinkCorrectAnswer} 1s linear infinite;
  `}
`;

const QuestionContainer = styled.div`
 width: 100%;
 background: linear-gradient(#100241, black);
 text-align: center;
 padding: 20px;
 border-radius: 10px;
 border: 2px solid white;
 font-size: 20px;
 margin-bottom: 20px;
`;

const Timer = styled.p`
 width: 70px;
 height: 70px;
 border-radius: 50%;
 border: 5px solid white;
 display: flex;
 align-items: center;
 justify-content: center;
 position: absolute;
 bottom: 80px;
 left: 80px;
 font-size: 30px;
 font-weight: 700;
`;

const JokerButton = styled.button<{ disabled: boolean; }>`
  margin: 2px;
  ${({ disabled }) => disabled && 'background: #8080809c; cursor: not-allowed;'}
`