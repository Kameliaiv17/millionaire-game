import { useNavigate } from "react-router-dom";
import { styled, keyframes } from "styled-components";
import { moneyPrizes } from "../utils";

interface Props {
  correctAnswers: number;
  gameOverReason: string;
}

const EndScreen: React.FC<Props> = ({ correctAnswers, gameOverReason }) => {
  const navigate = useNavigate();

  const handlePlayAgain = () => {
    navigate("/");
  }

  return (
    <EndScreenContainer>
      {correctAnswers === 15 ? (
        <div>
          <h1>Congratulations!</h1>
          <h2>You won $100,000</h2>
        </div>
      ) : (
        <div>
          <h1>Game Over!</h1>
          <h2>{gameOverReason}</h2>
          <p>Answered questions: {correctAnswers}</p>
        </div>
      )}
      <ResultTable>
        <tbody>
          {moneyPrizes.map((prize, i) => {
            const isReachedRow = 14 - i === correctAnswers - 1;

            if (isReachedRow) {
              return (
                <ReachedRow key={i}>
                  <td>{15 - i}: </td>
                  <td>${prize}</td>
                </ReachedRow>
              );
            }

            return (
              <tr key={i}>
                <td>{15 - i}: </td>
                <td>${prize}</td>
              </tr>
            );
          })}
        </tbody>
      </ResultTable>
      <button onClick={handlePlayAgain}>Play Again</button>
    </EndScreenContainer>
  );
};

export default EndScreen;

const EndScreenContainer = styled.div`
 min-height: 100vh;
 display: flex;
 flex-direction: column;
 align-items: center;
 justify-content: center;
 color: #ffff;
 `;

const blinkAnimation = keyframes`
0% { background: green; }
50% { background: orange; }
100% { background: green; }
`;

const ReachedRow = styled.tr`
  animation: ${blinkAnimation} 1s linear infinite;
  border: 2px solid black;
  border-radius: 5px; 
`;

const ResultTable = styled.table`
 width: 300px;
 border: 2px solid #ffff;
 border-radius: 10px;
 text-align: left;
 margin-bottom: 20px;
 backdrop-filter: blur(5px);

 tbody tr {
  transition: background-color 0.3s ease;
  
  td:first-child {
    border-right: none;
    border-radius: 5px 0 0 5px; 
  }

  td:last-child {
    border-left: none;
    border-radius: 0 5px 5px 0; 
  }
}
`;