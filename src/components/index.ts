import StartScreen from "./StartScreen";
import QuestionScreen from "./QuestionScreen";
import EndScreen from "./EndScreen";

export {
  StartScreen,
  QuestionScreen,
  EndScreen,
};