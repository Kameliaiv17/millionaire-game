import { useNavigate } from "react-router-dom";
import { Fragment, useState, useEffect } from "react";
import Swal from "sweetalert2";
import { keyframes, styled } from "styled-components";
import { difficulties } from "../utils";

interface Category {
  id: number,
  name: string,
}


const StartScreen: React.FC = () => {

  const navigate = useNavigate();
  const [categories, setCategories] = useState<Category[]>([]);
  const [selectedCategory, setSelectedCategory] = useState<string | null>(null);
  const [selectedDifficulty, setSelectedDifficulty] = useState<string | null>(null);

  useEffect(() => {
    fetch("https://opentdb.com/api_category.php")
      .then((res) => res.json())
      .then((data) => setCategories(data.trivia_categories));
  }, []);

  const handleStartGame = () => {
    if (selectedCategory && selectedDifficulty) {
      navigate("/questions", { state: { category: selectedCategory, difficulty: selectedDifficulty } });
    } else {
      const text = !selectedCategory?.length ? !selectedDifficulty?.length ? 'category and difficulty' : 'a category' : 'a difficulty';
      Swal.fire({
        icon: 'error',
        title: `Please select ${text}.`
      })
    }
  }

  return (
    <Fragment>
      <StartScreenContainer>
        <GameName>Who want's to be a Millionaire</GameName>
        <div>
          <p>Category:</p>
          <ButtonsContainer>
            <SectionSelect onChange={(e) => setSelectedCategory(e.target.value)}
              className="custom-select"
            >
              <option value="">Select Category</option>
              {categories.map((category: Category) => (
                <option key={category.id} value={category.id}>
                  {category.name}
                </option>
              ))}
            </SectionSelect>
          </ButtonsContainer>
        </div>
        <div>
          <p>Difficulty:</p>
          <ButtonsContainer>
            <SectionSelect onChange={(e) => setSelectedDifficulty(e.target.value)}
              className="custom-select"
            >
              <option value="">Select Difficulty</option>
              {difficulties.map((difficulty) => (
                <option key={difficulty} value={difficulty}>
                  {difficulty}
                </option>
              ))}
            </SectionSelect>
          </ButtonsContainer>
        </div>
        <StartBtn onClick={handleStartGame}>Start Game</StartBtn>
      </StartScreenContainer>
    </Fragment>
  )
};

export default StartScreen;

const blinkText = keyframes`
 0% {
  opacity: 0.7;
 }
 50% {
  opacity: 1;
 }
 100% {
  opacity: 0.7;
 }
`

const GameName = styled.h1`
   text-align: center;
 font-family: Verdana;
 color: #ffc310;
 animation: ${blinkText} 1.9s linear infinite;
 font-size: 3em;
 text-shadow: 0 0 3px #080808, 0 0 5px #fd1b1b;

`;

const StartScreenContainer = styled.div`
 min-height: 100vh;
 display: flex;
 justify-content: center;
 align-items: center;
 flex-direction: column;
`;

const ButtonsContainer = styled.div`
 display: flex;
 justify-content: center;
 flex-wrap: wrap;
 gap: 10px;
 margin-bottom: 10px;
 button.selected {
  border-color: #646cff
 }
`;

const StartBtn = styled.button`
  width: 300px;
`;

const SectionSelect = styled.select`
  width: 300px;
  border-radius: 8px;
  text-align: center;
  border: 1px solid transparent;
  padding: 0.6em 1.2em;
  font-size: 1em;
  font-weight: 500;
  font-family: inherit;
  background-color: #1a1a1a;
  color: #ffff;
  cursor: pointer;
  transition: border-color 0.25s;


  &:hover {
    border-color: #646cff;
  }

  &:focus,
  &:focus-visible {
    outline: 4px auto -webkit-focus-ring-color;
  }
`;
