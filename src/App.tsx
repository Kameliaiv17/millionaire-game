import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import './App.css';
import { StartScreen,QuestionScreen,EndScreen } from "./components";


const App: React.FC = () => {

  return (
    <Router>
      <Routes>
        <Route path="/" element={<StartScreen />} />
        <Route path="questions" element={<QuestionScreen/>} />
        <Route path="/end" element={<EndScreen correctAnswers={0} gameOverReason={""} />} />
      </Routes>
    </Router>
  )
}

export default App;


