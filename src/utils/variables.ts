export const moneyPrizes = [
  100000, 50000, 30000, 20000, 10000, 5000, 3000, 2000, 1500, 1000, 500, 400,
  300, 200, 100,
];
export const difficulties = ["easy", "medium", "hard"];
